import webbrowser
from urllib.parse import parse_qs
import vk
import time

APP_ID = 5952326

class VkUser:
    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name

    @classmethod
    def convert(cls, user_dict):
        first_name = user_dict['first_name']
        last_name = user_dict['last_name']
        return cls(first_name, last_name)

    def __str__(self):
        return self.first_name + ' ' + self.last_name


def get_auth_params():
    auth_url = ("https://oauth.vk.com/authorize?client_id={app_id}"
                "&scope=friends&redirect_uri=http://oauth.vk.com/blank.html"
                "&display=page&response_type=token".format(app_id=APP_ID))
    webbrowser.open_new_tab(auth_url)
    redirected_url = input("Вставьте адресную строку из браузера для получения токена:\n")
    aup = parse_qs(redirected_url)
    aup['access_token'] = aup.pop(
        'https://oauth.vk.com/blank.html#access_token')
    return aup['access_token'][0], aup['user_id'][0]


def get_auth_api(access_token):
    session = vk.Session(access_token=access_token)
    return vk.API(session)


def get_no_auth_api():
    session = vk.Session()
    return vk.API(session)


def humanize(vk_auth_api, users_list):
    vk_users = []
    time.sleep(1)
    user_dicts = vk_auth_api.users.get(user_ids=users_list)
    for user in user_dicts:
        vk_users.append(str(VkUser.convert(user)))
    return vk_users


def find_friends_of_user_friends(user_id):
    users_list = {}
    for user_id in my_friends:
        try:
            result = vk_auth_api.friends.get(user_id=user_id)
            time.sleep(1)
        except vk.exceptions.VkAPIError as er:
            print(er)
        users_list[user_id] = result
    return users_list


def search_shared_users(vk_auth_api, user_list):
    for user, users in users_list.items():
        users_set = set(users)
        for user_s, users_s in users_list.items():
            if user != user_s:
                users_s_set= set(users_s)
                result = users_set & users_s_set
                if len(result) > 0:
                    # time.sleep(1)
                    print('Для пользователя {} и {} общие друзья {}'.format(humanize(vk_auth_api, user)[0], humanize(vk_auth_api, user_s)[0], humanize(vk_auth_api, result)))


#start main programm
token, user_id = get_auth_params()
vk_auth_api = get_auth_api(token)
print('Сессия авторизована, ищем наших друзей...')
my_friends = vk_auth_api.friends.get(user_id=user_id)
print('Получили список наших друзей, приступаем к поиску друзей у наших друзей')
users_list = find_friends_of_user_friends(user_id)
print('Завершили поиск друзей, добавим в дерево наших друзей')
users_list[user_id] = humanize(vk_auth_api, my_friends)
print('Начинаем поиск общих друзей')
search_shared_users(vk_auth_api, users_list)
# print('Найдены пересечения для {} пользователей'.format(len(users_list)))
